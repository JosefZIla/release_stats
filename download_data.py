#!/usr/bin/python
import gitlab
import sys
import csv
from dateutil.parser import parse

GOODDATA_GROUP_ID = 5818797
ISSUES_HEADER = ['iid', 'id', 'url', 'title', 'project_id', 'created_at', 'closed_at', 'updated_at', 'assignee_id', 'milestone_id', 'closed_by']
MILESTONES_HEADER = ['iid', 'id', 'title', 'project_id', 'state', 'due_date', 'created_at', 'updated_at']
LABELS_HEADER = ['id', 'name']
LABEL_MAPPINGS_HEADER = ['issue_id', 'label_id']
PROJECTS_HEADER = ['id', 'name', 'visibility', 'forks_count', 'is_fork', 'import_status', 'open_issues_count', 'web_url', 'mirror', 'default_branch']

update_date = None
milestones = []
label_lists = {}
labels = []
label_mapping = []

#get date from command line parameter
try:
	update_date = sys.argv[1]
	parse(update_date)
except ValueError:
	print("ERROR: first argument is not a valid date")
except IndexError:
	print("ERROR: date for the data download needs to be provided as a first argument")

#connect to github - should load token from vault
gl = gitlab.Gitlab.from_config('gooddata', ['/home/josef/.python-gitlab.cfg'])

gooddata = gl.groups.get(GOODDATA_GROUP_ID)

def add_milestone(milestone):
	if milestone not in milestones:
		milestones.append(milestone)

def get_project_labels(project):
	label_lists[project.id] = gl.projects.get(project.id).labels.list(all=True)

def get_group_labels(group):
	label_lists['group'] = group.labels.list(all=True)

def search_for_label(label,label_list):
	return list(filter(lambda l: l.name == label, label_list))

def add_label(issue,label):
	project_labels = label_lists[issue.project_id]
	group_labels = label_lists['group']
	found = search_for_label(label, project_labels)
	if found:
		l_data = [found[0].id, found[0].name]
		if l_data not in labels:
			labels.append(l_data)
		label_mapping.append([issue.id, found[0].id])
	else:
		found = search_for_label(label,group_labels)
		if found:
			l_data = [found[0].id, found[0].name]
			if l_data not in labels:
				labels.append(l_data)
			label_mapping.append([issue.id, found[0].id])
		else:
			print('WARNING: label '+label+' not found in project or group, ignoring this label')

#process projects
projects = gooddata.projects.list(simple=False)

with open('gitlab_projects.csv',mode='w') as projects_f:
	projects_w = csv.writer(projects_f, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
	projects_w.writerow(PROJECTS_HEADER)
	for project in projects:
		project_data = []
		project_data.append(project.id)										# project id
		project_data.append(project.name)									# project name
		project_data.append(project.visibility)								# project visibility
		project_data.append(project.forks_count)							# project forks count
		project_data.append(not project.namespace['path'] == 'gooddata')	# project is a fork (not in gooddata namespace)
		project_data.append(project.import_status)							# project import status
		if(project.issues_enabled):
			project_data.append(project.open_issues_count)					# project open issues count
			get_project_labels(project)
		else:
			project_data.append(None)
		project_data.append(project.web_url)								# project web url
		if(hasattr(project, 'mirror')):
			project_data.append(project.mirror)								# project is a mirror
		else:
			project_data.append(False)
		project_data.append(project.default_branch)							# project default branch
		projects_w.writerow(project_data)

get_group_labels(gooddata)

#process issues
issues = gooddata.issues.list(updated_after=update_date,all=True)

with open('gitlab_issues.csv',mode='w') as issue_f:
	issue_w = csv.writer(issue_f, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
	issue_w.writerow(ISSUES_HEADER)
	for issue in issues:
		issue_data = []
		issue_data.append(issue.iid)					# unique id
		issue_data.append(issue.id)						# unique id
		issue_data.append(issue.web_url)				# issue URL
		issue_data.append(issue.title)					# issue title
		issue_data.append(issue.project_id)				# issue description
		issue_data.append(issue.created_at)				# issue creation date
		issue_data.append(issue.closed_at)				# issue creation date
		issue_data.append(issue.updated_at)				# issue creation date
		if(issue.assignee):
			issue_data.append(issue.assignee['id'])		# issue assignee
			# we can add user here
		else:
			issue_data.append(None)
		if(issue.milestone):
			issue_data.append(issue.milestone['id'])	# issue milestone
			add_milestone(issue.milestone)
		else:
			issue_data.append(None)
		if(issue.closed_by):
			issue_data.append(issue.closed_by['id'])	# issue closed by
			# we can add user here
		else:
			issue_data.append(None)
		for label in issue.labels:						# labels
			add_label(issue,label)
		issue_w.writerow(issue_data)

#process milestones
with open('gitlab_milestones.csv',mode='w') as milestone_f:
	milestone_w = csv.writer(milestone_f, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
	milestone_w.writerow(MILESTONES_HEADER)
	for milestone in milestones:
		milestone_data = []
		milestone_data.append(milestone['iid'])				# milestone iid
		milestone_data.append(milestone['id'])				# milestone id
		milestone_data.append(milestone['title'])			# milestone title
		if('project_id' in milestone.keys()):
			milestone_data.append(milestone['project_id'])	# milestone project id
		else:
			milestone_data.append(None)
		milestone_data.append(milestone['state'])			# milestone state
		if('due_date' in milestone.keys()):
			milestone_data.append(milestone['due_date'])	# milestone due date
		else:
			milestone_data.append(None)
		milestone_data.append(milestone['created_at'])		# milestone created_at
		milestone_data.append(milestone['updated_at'])		# milestone updated_at
		milestone_w.writerow(milestone_data)

#process labels

with open('gitlab_labels.csv',mode='w') as label_f:
	label_w = csv.writer(label_f, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
	label_w.writerow(LABELS_HEADER)
	for label in labels:
		label_w.writerow(label)							# label

#process label mappings
with open('gitlab_label_mappings.csv',mode='w') as label_map_f:
	label_map_w = csv.writer(label_map_f, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
	label_map_w.writerow(LABEL_MAPPINGS_HEADER)
	for mapping in label_mapping:
		label_map_w.writerow(mapping)						# mapping of label to issue

