#!/usr/bin/python
import gitlab

code_drop = '2021-03-29'
release_date = '2021-04-11'
GA_milestone_iid = 5 # r1.0.0 Tiger GA
project_id = 16539767 # gooddata/gdc-nas
issues_total = 0
issues_fixed = 0
reporters = {}

gl = gitlab.Gitlab.from_config('gooddata', ['/home/josef/.python-gitlab.cfg'])

project = gl.projects.get(project_id)

issues = project.issues.list(created_after=code_drop, created_before=release_date, all=True)

for issue in issues:
  if('Bug' in issue.labels):
    print('https://gitlab.com/gooddata/gdc-nas/-/issues/'+str(issue.iid),end='')
    if(issue.author['name'] in reporters):
      reporters[issue.author['name']] += 1
    else:
      reporters[issue.author['name']] = 1
    issues_total += 1
    if (issue.milestone): 
      if (issue.state == 'closed' and issue.milestone['iid'] == GA_milestone_iid):
        issues_fixed += 1
        print(' - fixed before release',end='')
    print()

print('Total \'Bug\' tickets reported after code drop: '+str(issues_total))
print('Total \'Bug\' tickets fixed before release: '+str(issues_fixed))
print(reporters)
